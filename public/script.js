
navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

var myContextId = getParameterByName('r');
var myUserId = getParameterByName('u');
var WSS_HOST = 'wss://'+window.location.hostname+':8000?u='+myUserId+'&r='+myContextId;
var HTTP_HOST = window.location.origin;
var webSocketConnect = new WebSocketConnect(WSS_HOST);

var $userList = $('#users-list');
var $localVideoWrapper = $('#localVideo');
var $remoteVideoWrapper = $('#remoteVideo');
var $joinButton = $('#join');
var $callButton = $('#call');
var $endCallButton = $('#end');

var users = {};

$(function() {
	$('#chat-id').text(myContextId);
	$('#user-id').text(myUserId);
	getContextUsers();
});

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	url = url.toLowerCase(); // This is just to avoid case sensitiveness
	name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return escapeHtml(decodeURIComponent(results[2].replace(/\+/g, " ")));
}

function WebSocketConnect(url) {
	this.connect = new WebSocket(url);
	this.channels = {};

	this.connect.onmessage = function(message) {
		//console.log('Got WebSocket event: ', message.data);
		var data = JSON.parse(message.data);

		if (this.channels[data.channel]) {
			this.channels[data.channel].forEach(function(element) {
				element.call(null, data);
			})
		}
	}.bind(this);
}

WebSocketConnect.prototype.subscribe = function(channelName, cb) {
	if (this.channels[channelName]) {
		return this.channels[channelName].push(cb);
	}
	this.channels[channelName] = [cb];
};

WebSocketConnect.prototype.unsubscribe = function(channelName) {
	delete this.channels[channelName];
};

WebSocketConnect.prototype.send = function(channelName, subject, data) {
	this.connect.send(JSON.stringify({
		channel: channelName,
		subject: subject,
		params: data
	}));
};

function $http(url){

	// A small example of object
	var core = {

		// Method that performs the ajax request
		ajax : function (method, url, args) {

			// Creating a promise
			var promise = new Promise( function (resolve, reject) {

				// Instantiates the XMLHttpRequest
				var client = new XMLHttpRequest();
				var uri = url;

				if (args && (method === 'POST' || method === 'PUT')) {
					uri += '?';
					var argcount = 0;
					for (var key in args) {
						if (args.hasOwnProperty(key)) {
							if (argcount++) {
								uri += '&';
							}
							uri += encodeURIComponent(key) + '=' + encodeURIComponent(args[key]);
						}
					}
				}

				client.open(method, uri);
				client.send();

				client.onload = function () {
					if (this.status >= 200 && this.status < 300) {
						// Performs the function "resolve" when this.status is equal to 2xx
						var res = '';
						if (this.response) {
							res = JSON.parse(this.response);
						}
						resolve(res);
					} else {
						// Performs the function "reject" when this.status is different than 2xx
						reject(this.statusText);
					}
				};
				client.onerror = function () {
					reject(this.statusText);
				};
			});

			// Return the promise
			return promise;
		}
	};

	// Adapter pattern
	return {
		'get' : function(args) {
			return core.ajax('GET', url, args);
		},
		'post' : function(args) {
			return core.ajax('POST', url, args);
		},
		'put' : function(args) {
			return core.ajax('PUT', url, args);
		},
		'delete' : function(args) {
			return core.ajax('DELETE', url, args);
		}
	};
}

webSocketConnect.subscribe('context.'+myContextId, function(event) {
	switch (event.subject) {
		case 'context.user.online':
			callUserOnlineHandler(event.params.userId);
			break;
		case 'context.user.offline':
			callUserOfflineHandler(event.params.userId);
			break;
	}
});

function callUserOnlineHandler(userId) {
	addUserInList(userId);
}

function callUserOfflineHandler(userId) {
	if (users[userId]) {
		delete users[userId];
		$userList.children('#'+userId).remove();
	}
}

function addUserInList(userId) {
	if (userId === myUserId || users[userId]) {
		return;
	}
	users[userId] = true;
	$('<li id="'+userId+'"><a href="#">'+userId+'</a></li>').appendTo($userList);
}

function getContextUsers() {
	$http(HTTP_HOST+'/contexts/'+myContextId+'/users')
		.get()
		.then(function(users) {
			users.forEach(addUserInList);
		})
		.catch(function(err) {
			console.log(err);
		});
}

function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
}
