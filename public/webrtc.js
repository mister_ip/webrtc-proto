
var callParticipants = {};
var peerConnectionConfig;
var localStream;
var localStreamView;
var inCall;

webSocketConnect.subscribe('context.' + myContextId, callHandler);
webSocketConnect.subscribe('user.' + myUserId, userEventHandler);

getCallStatus();

function call() {
    var callParams = {
        userId: myUserId,
        video: true,
        audio: true
    };

    $http(HTTP_HOST+'/contexts/'+myContextId+'/rtc/call')
        .post(callParams)
        .then(getConstraints)
        .then(createLocalStream)
        .then(createLocalStreamView)
        .catch(errorHandler);

    inCall=true;
    $endCallButton.show();
    $callButton.hide();
}

function join() {
    $http(HTTP_HOST+'/contexts/'+myContextId+'/rtc/call/participant')
        .post({
            userId: myUserId
        })
        .then(getConstraints)
        .then(createLocalStream)
        .then(createLocalStreamView)
        .catch(errorHandler);
    
    inCall=true;
    $endCallButton.show();
    $joinButton.hide();
}

function endCall() {
    $http(HTTP_HOST+'/contexts/'+myContextId+'/rtc/call/participants/'+myUserId).delete();
    destroyAllParticipantsStream();
    stopLocalStream();
    getCallStatus();
    $endCallButton.hide();
    inCall = false;
}

function getCallStatus() {
    $http(HTTP_HOST+'/contexts/'+myContextId+'/rtc/call')
        .get()
        .then(function() {
            $joinButton.show();
        })
        .catch(function() {
            $callButton.show();
            $endCallButton.hide();
        });
}

function callHandler(event) {
    switch (event.subject) {
        case 'call.created':
            callCreatedHandler(event.params);
            break;
        case 'call.updated':
            callUpdatedHandler(event.params);
            break;
        case 'call.deleted':
            callEndHandler(event.params);
            break;
    }
}

function callCreatedHandler(call) {
    if (call.owner === myUserId) {
        return;
    }

    $joinButton.show();
    $callButton.hide();
}

function callUpdatedHandler(changes) {
    switch (changes.op) {
        case 'userLeft':
            userLeftHandler(changes.userId);
            break;
        case 'userAdd':
            userAddHandler(changes.userId);
            break;
    }
}

function callEndHandler() {
    if (inCall) {
        stopLocalStream();
        destroyAllParticipantsStream();
        inCall = false;
    }
    $joinButton.hide();
    $endCallButton.hide();
    $callButton.show();
}

function userLeftHandler(userId) {
    if (callParticipants[userId]) {
        callParticipants[userId].view.remove();
        callParticipants[userId].connect.close();
        delete callParticipants[userId];
    }
}

function userAddHandler(userId) {
    console.log('user add: %s', userId);
    if (userId !== myUserId && inCall) {
        createRTCPeerConnection({userId: userId}, peerConnectionConfig, true);
    }
}

function createRTCPeerConnection(params, peerConnectionConfig, isCaller) {
    console.log('Create connection to user: %s', params.userId);
    var channel = 'user.'+params.userId;

    localStream.then(function(stream) {
        var peerConnection = new RTCPeerConnection(peerConnectionConfig);

        peerConnection.onicecandidate = gotIceCandidate.bind(null, channel);
        peerConnection.onaddstream = gotRemoteStream.bind(null, params.userId);
        peerConnection.addStream(stream);

        if(isCaller) {
            createOffer(peerConnection, channel);
        }
        else {
            createAnswer(peerConnection, params);
        }

        callParticipants[params.userId] = {};
        callParticipants[params.userId].connect = peerConnection;
    });
}

function userEventHandler(event) {
    if (!inCall) {
        return;
    }
    switch (event.subject) {
        case 'signal.offer':
            signalSdpOfferHandler(event.params);
            break;
        case 'signal.answer':
            signalSdpAnswerHandler(event.params);
            break;
        case 'signal.ice':
            signalIceHandler(event.params);
            break;
    }
}

function signalSdpOfferHandler(params) {
    if (params.userId === myUserId) {
        return;
    }
    console.log('got offer from: %s', params.userId);
    addCallParticipant(params);
}

function signalSdpAnswerHandler(params) {
    console.log('got answer from: %s', params.userId);
    if (callParticipants[params.userId]) {
        return callParticipants[params.userId]
            .connect.setRemoteDescription(new RTCSessionDescription(params.sdp));
    }
    console.error('Not found participant %', params.userId);
}

function signalIceHandler(params) {
    console.log('got signal.ice from %s', params.userId);
    if (callParticipants[params.userId]) {
        return callParticipants[params.userId]
                    .connect.addIceCandidate(new RTCIceCandidate(params.ice));
    }
    console.error('IceHandler: not found participant %s', params.userId);
}

function createOffer(peerConnection, channel) {
    console.log('create offer for user: %s', channel);
    peerConnection.createOffer(function(description) {
        peerConnection.setLocalDescription(description, function () {
            webSocketConnect.send(channel, 'signal.offer', {
                'sdp': description,
                'userId': myUserId
            })
        }, function() {console.error('set Offer description error')});
    }, errorHandler);
}

function createAnswer(peerConnection, params) {
    var channel = 'user.'+params.userId;
    console.log('create answer for: %s', channel);
    peerConnection.setRemoteDescription(new RTCSessionDescription(params.sdp), function() {
        peerConnection.createAnswer(function(description){
            peerConnection.setLocalDescription(description, function () {
                webSocketConnect.send(channel, 'signal.answer', {
                    'sdp': description,
                    'userId': myUserId
                })
            }, function() {console.error('set Answer description error')});
        }, errorHandler);
    }, errorHandler);
}

function gotIceCandidate(channel, event) {
    if(event.candidate != null) {
        console.log('send ice candidate for %s', channel);
        webSocketConnect.send(channel, 'signal.ice', {
            'ice': event.candidate,
            'userId': myUserId
        })
    }
}

function gotRemoteStream(userId, event) {
    console.log('gotRemoteStream from %s', userId);
    callParticipants[userId].view = createRemoteStreamView(event.stream);
}

function stopLocalStream() {
    if (localStreamView) {
        localStreamView.remove();
        localStreamView = null;
    }

    if (localStream) {
        localStream.then(function(stream){
            destroyStream(stream);
        });
        localStream = null;
    }
}

function destroyStream(stream) {
    var tracks = stream.getTracks();

    tracks.forEach(function(track) {
        track.stop();
    })
}

function addCallParticipant(params) {
    if (params.userId === myUserId) {
        return;
    }
    createRTCPeerConnection(params, peerConnectionConfig);
}

function destroyAllParticipantsStream() {
    for (var userId in callParticipants) {
        destroyParticipantStreamById(userId);
    }
}

function destroyParticipantStreamById(userId) {
    if (callParticipants[userId]) {
        callParticipants[userId].view.remove();
        callParticipants[userId].connect.close();
        delete callParticipants[userId];
        webSocketConnect.unsubscribe('user.'+userId);
    }
}

function createLocalStreamView(stream) {
    if (!localStreamView) {
        localStreamView = $('<video />', {
            autoplay: true,
            muted: true,
            src: window.URL.createObjectURL(stream)
        }).css({width:'40%'}).appendTo($localVideoWrapper);
    }

    return localStreamView;
}

function createRemoteStreamView(stream) {
    return $('<video />',{
        autoplay: true,
        src: window.URL.createObjectURL(stream)
    }).css({width:'40%'}).appendTo($remoteVideoWrapper);
}

// options = {video: true, audio: true}
function createLocalStream(options) {
    if (options.video) {
        options.video = {
            mandatory: {
                maxWidth: 160,
                maxHeight: 160
            }
        }
    }
    return localStream = new Promise(function(resolve, reject) {
        if (navigator.getUserMedia) {
            console.log(options);
            navigator.getUserMedia(options, function(stream) {
                resolve(stream);
            }, reject);
        } else {
            reject(new Error('Your browser does not support getUserMedia API'));
        }
    });
}

function getConstraints(res) {
    peerConnectionConfig = {iceServers: res.iceServers};
    return {
        video: res.constraints.video === 'true',
        audio: res.constraints.audio === 'true'
    }
}

function errorHandler(error) {
    console.error(error);
}
