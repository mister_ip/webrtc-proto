'use strict';

var https = require('https');
var fs = require('fs');
var querystring = require('querystring');

var WebSocketServer = require('ws').Server;
var express = require('express');
var bodyParser = require('body-parser');

const OPTIONS = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

const SERVER_PORT = 8000;

var app = express();
var httpsServer = https.createServer(OPTIONS, app);

var contexts = {};

app.use(function(req, res, next) {
    console.log('%s %s', req.method, req.path);
    next();
});

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/../public'));

app.get('/contexts', function (req, res) {
    res.json(Object.keys(contexts).map(function(key){
        return contexts[key];
    }));
});
app.get('/contexts/:id', function (req, res) {
    var id = req.params.id;

    if (contexts[id]) {
        return res.json(contexts[id])
    }
    res.status(404).end();
});
app.get('/contexts/:id/users', function (req, res) {
    var id = req.params.id;

    if (contexts[id]) {
        return res.json(contexts[id].users || []);
    }
    res.status(404).end();
});

app.get('/contexts/:id/rtc/call', function (req, res) {
    var contextId = req.params.id;

    if (contexts[contextId] && contexts[contextId].call) {
        return res.json(contexts[contextId].call);
    }
    res.status(404).end();
});
app.post('/contexts/:id/rtc/call', function (req, res) {
    var contextId = req.params.id;

    if (contexts[contextId] && contexts[contextId].call) {
        return res.status(409).end();
    }

    contexts[contextId].call = {
        constraints: {
            video: req.query.video,
            audio: req.query.audio
        },
        participants: [req.query.userId],
        owner: req.query.userId,
        createTime: Date.now(),
        iceServers: [
            {url: 'stun:stun.services.mozilla.com'},
            {url: 'stun:stun.l.google.com:19302'}
        ]
    };

    res.json(contexts[contextId].call);
    wss.broadcast({
        channel: 'context.'+contextId,
        subject: 'call.created',
        params: contexts[contextId].call
    });
});
app.post('/contexts/:id/rtc/call/participant', function (req, res) {
    var contextId = req.params.id;
    var userId = req.query.userId;

    if (!contexts[contextId] && !contexts[contextId].call) {
        return res.status(404).end();
    }

    if (!contexts[contextId].call.startTime) {
        contexts[contextId].call.startTime = Date.now();
    }

    contexts[contextId].call.participants.push(userId);

    res.json(contexts[contextId].call);
    wss.broadcast({
        channel: 'context.'+contextId,
        subject: 'call.updated',
        params: {
            op: 'userAdd',
            userId: userId
        }
    });
});
app.delete('/contexts/:contextId/rtc/call/participants/:userId', function (req, res) {
    var contextId = req.params.contextId;
    var userId = req.params.userId;

    deleteUserFromCall(contextId, userId);
    res.status(202).end();
});

// WS
var wss = new WebSocketServer({server: httpsServer});

wss.broadcast = function(data) {
    if (typeof data !== 'string') {
        data = JSON.stringify(data);
    }

    for(var i in this.clients) {
        this.clients[i].send(data);
    }
};

wss.on('connection', function(ws) {
    var params = querystring.parse(ws.upgradeReq.url.replace('/?',''));
    var contextId = params.r;
    var userId = params.u;

    ws.on('message', function(message) {
        console.log('received: %s', message);
        wss.broadcast(message);
    });
    ws.on('close', function() {
        deleteUserFromCall(contextId, userId);
        userOffline(contextId, userId);
    });
    userOnline(contextId, userId);
});

httpsServer.listen(SERVER_PORT, function() {
    console.log('Server ready in %d port', SERVER_PORT);
});

function userOnline(contextId, userId) {
    if (!contexts[contextId]) {
        contexts[contextId] = {};
    }
    if (!contexts[contextId].users) {
        contexts[contextId].users = [];
    }
    if (contexts[contextId].users.indexOf(userId) === -1) {
        contexts[contextId].users.push(userId);
    }
    wss.broadcast({
        channel: 'context.'+contextId,
        subject: 'context.user.online',
        params: {
            userId: userId
        }
    });
}

function userOffline(contextId, userId) {
    console.log(contextId, userId);
    if (!contexts[contextId] || !contexts[contextId].users) {
        return;
    }
    var index = contexts[contextId].users.indexOf(userId);

    if (index > -1) {
        contexts[contextId].users.splice(index, 1);
        wss.broadcast({
            channel: 'context.'+contextId,
            subject: 'context.user.offline',
            params: {
                userId: userId
            }
        });
    }
}

function deleteUserFromCall(contextId, userId) {
    if (contexts[contextId] && contexts[contextId].call) {
        var index = contexts[contextId].call.participants.indexOf(userId);

        if (index > -1) {
            var message;
            contexts[contextId].call.participants.splice(index, 1);

            if (contexts[contextId].call.participants.length > 1) {
                message = {
                    channel: 'context.'+contextId,
                    subject: 'call.updated',
                    params: {
                        op: 'userLeft',
                        userId: userId
                    }
                };
            }
            else {
                message = {
                    channel: 'context.'+contextId,
                    subject: 'call.deleted',
                    params: {
                        contextId: contextId
                    }
                };
                delete contexts[contextId].call;
            }
            wss.broadcast(message);
        }
    }
}
